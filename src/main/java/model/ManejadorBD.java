package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Andrea
 */
public class ManejadorBD {
    private Connection conexion;
    private Statement sentencia;
    public ManejadorBD(){
        try{
            Class.forName("com.mysal.jdbc.Driver");
            conexion = (Connection) DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/u3bd1",
                    "root","root");
            sentencia = conexion.createStatement();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void cerrarConexion() throws Exception{
    if(sentencia!=null)sentencia.close();
    if(conexion!=null)conexion.close();
}
    public ArrayList<String> consultar()throws Exception{
        String qry = "SELECT * FROM catalogo";
        ResultSet rs = sentencia.executeQuery(qry);
        ArrayList<String> resultados =new ArrayList<>();
        while(rs.next()){
            int id = rs.getInt("id");
            String desc =rs.getString("descripcion");
            
            resultados.add("id:"+id +""+"descripcion:"+desc);
        }
        rs.close();
        return resultados;
    }
    public int insertar(String nuevoDato) throws Exception{
        String qry = "INSERT INTO catalogo VALUES(NULL,'"+nuevoDato+"')";
        return sentencia.executeUpdate(qry);
    }
    public int eliminar(String nuevoDato) throws Exception{
        String qry = "DELETE INTO catalog  VALUES(NULL, '"+nuevoDato+"')"; 
        return sentencia.executeUpdate(qry);
    }
    public int modificar(String nuevoDato) throws Exception{
        String qry = "UPDATE INTO catalog  VALUES(NULL, '"+nuevoDato+"')";
        return sentencia.executeUpdate(qry);
    }
}
